<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InOutHome extends CI_Controller {
	var $data;
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('InOutHomeModel','hv');
		$this->data['controller'] = 'InOutHome';
		$this->data['home'] = 'People In-Out';
	}

	public function index()
	{
		$data['active'] = 'inout_dash';
		$data = array_merge($data,$this->data);
		$this->load->view('inOut/home',$data);
	}

	/**
	 * method for add all basic components needed
	 * basic configuration settings field add function
	 */
	function common_settings(){
		$key_data = $this->hv->select_key_data();
		$data = [
			'title'	=> 'Common Settings',
			'action' => 'List',
			'active' => 'inout_comm',
			'list_data' => $key_data,
			'additional_js' => ['bootstrap-tagsinput.js'],
			'additional_css' => ['bootstrap-tagsinput.css','custom_style.css']
			];
			$data = array_merge($data,$this->data);
		$this->load->view('inOut/common_settings',$data);
	}

	/**
	 * save common setings
	 * @param parametername,value
	 */
	public function save_common_settings () {

		$post_data = $this->input->post();
		$this->hv->save_common_settings($post_data);
	}

	/**
	 * add screen for adding camera
	 */
	public function add_camera($edit = null) {
		$location = explode(',',$this->hv->get_settings('Location'));
		$location_type =explode(',',$this->hv->get_settings('Location_type'));
		$brand =explode(',',$this->hv->get_settings('Brand'));
		$protocol =explode(',',$this->hv->get_settings('Protocol'));
		$data = [
			'active' => 'inout_cam_add',
			'title'	=> 'Camera',
			'action' => 'Add',
			'location' => $location,
			'location_type' => $location_type,
			'brand' => $brand,
			'protocol' => $protocol,
		];
		if($edit != null) {
			$data['fetch_data'] = $this->hv->select_camera($edit);
		}
		$data = array_merge($data,$this->data);
		$this->load->view('inOut/add_camera',$data);
	}

	/**
	 * save camera fields
	 * @param camera_name,location,brand,location_type,protocol
	 */
	public function save_camera() {
		$post_data = $this->input->post();
		$return  = $this->hv->save_camera($post_data);
		if($return==true) {
			redirect(base_url('/InOutHome/camera'));
		}else{
			$this->load->view('inOut/add_camera',$data);
		}
	}

	/**
	 * list screen for camera
	 */
	public function camera() {
		$list_data = $this->hv->select_camera();
		$data = [
			'active' => 'inout_cam',
			'title'	=> 'Camera',
			'action' => 'List',
			'list_data' => $list_data
		];
		$data = array_merge($data,$this->data);
		$this->load->view('inOut/camera',$data);
	}

	/**
	 * delete camera
	 */
	public function delete_camera($id) {
		$this->hv->delete_camera($id);
		redirect(base_url('/InOutHome/camera'));
	}
}
