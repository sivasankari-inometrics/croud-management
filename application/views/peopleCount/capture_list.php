<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
					<!-- [ breadcrumb ] start -->
				<div class="page-header">
					<div class="page-block">
						<div class="row align-items-center">
							<div class="col-md-12">
								<div class="page-header-title">
										<h5 class="m-b-10"><?= $title ?></h5>
								</div>
								<ul class="breadcrumb">
										<li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
										<li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
					<!-- [ breadcrumb ] end -->
				<div class="main-body">
					<div class="page-wrapper">
							<!-- [ Main Content ] start -->
						<div class="row">
							<div class="col-sm-12">
								<div class="card">
									<div class="card-header">
											<h5><?= $title.' - '.$action ?></h5>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-offset-4 col-md-8">



											<div class="card-block px-0 py-3">
                                            <div class="table-responsive">

																	<table class="table table-hover">
																			<tbody>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-1.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Isabella Christensen</h6>
																									<p class="m-0">Lorem Ipsum is simply…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>11 MAY 12:56</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-2.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Mathilde Andersen</h6>
																									<p class="m-0">Lorem Ipsum is simply text of…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-red f-10 m-r-15"></i>11 MAY 10:35</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-3.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Karla Sorensen</h6>
																									<p class="m-0">Lorem Ipsum is simply…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>9 MAY 17:38</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-1.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Ida Jorgensen</h6>
																									<p class="m-0">Lorem Ipsum is simply text of…</p>
																							</td>
																							<td>
																									<h6 class="text-muted f-w-300"><i class="fas fa-circle text-c-red f-10 m-r-15"></i>19 MAY 12:56</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																					<tr class="unread">
																							<td><img class="rounded-circle" style="width:40px;" src="assets/images/user/avatar-2.jpg" alt="activity-user"></td>
																							<td>
																									<h6 class="mb-1">Albert Andersen</h6>
																									<p class="m-0">Lorem Ipsum is simply dummy…</p>
																							</td>
																							<td>
																									<h6 class="text-muted"><i class="fas fa-circle text-c-green f-10 m-r-15"></i>21 July 12:56</h6>
																							</td>
																							<td><a href="#!" class="label theme-bg2 text-white f-12">Reject</a><a href="#!" class="label theme-bg text-white f-12">Approve</a></td>
																					</tr>
																			</tbody>
																	</table>
															</div>
													</div>




													<button  class="btn btn-primary" id="button">Submit</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
							<!-- [ Main Content ] end -->
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
