<?php
include('application/views/include/header.php');
include('application/views/include/sidebar.php');
?>

<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $title ?></h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href=""><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="javascript:"><?= $home.' / '.$title ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5><?= $title.' - '.$action ?></h5>
                                        </div>
                                        <!-- [ stiped-table ] start -->
                                        <div class="card-block table-border-style">
                                            <div class="table-responsive">
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Parameters</th>
                                                            <th>Values</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
													 if(isset($list_data)) {
														$no = 1;
														foreach($list_data as $tbl_data) {
															?>
                                                        <tr>
                                                            <th scope="row"><?= $no++ ?></th>
                                                            <td><?= $tbl_data->key_field ?><input type="hidden" name="<?= $tbl_data->key_field ?>" value="<?= $tbl_data->key_field ?>" class="parameter"></td>
                                                            <td><p><?= $tbl_data->key_value ?></p><input type="text" class="form-control"   placeholder="Enter values" name ="key_value" data-role="tagsinput"  value="<?= $tbl_data->key_value ?>"><small id="emailHelp" class="form-text text-muted hidden_input">Type your Value and press enter to add values.</small></td>
                                                            <td><a  class="label theme-bg text-white f-12 add_multi_input"><i class="feather icon-plus"></i>Add</a><a  class="label theme-bg text-white f-12 save_multi_input hidden_input"><i class="feather icon-check-circle"></i>Save</a><a href="<?=base_url($controller.'/common_settings')?>" class="label theme-bg2 text-white f-12 hidden_input"><i class="feather icon-x"></i></a></td>

                                                        </tr>
														<?php
														}
													} ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                <!-- [ stiped-table ] end -->
                                    </div>
                                </div>
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<!-- [ Main Content ] end -->
	<?= include('application/views/include/footer.php'); ?>
	<script>
	   $('.add_multi_input').on('click',function(){
		$(this).closest('tr').find('.hidden_input').show();
		$(this).closest('tr').find('.bootstrap-tagsinput').show();
		$(this).closest('tr').find('.add_multi_input').hide();
		$(this).closest('tr').find('p').hide();
	   });
	</script>
	<script>
		$('.save_multi_input').on('click',function(){
		  parameter_value = $(this).closest('tr').find("input[name=key_value]").val();
		  parameter = $(this).closest('tr').find(".parameter").val();
		  $.ajax({
			type: 'POST',
			url: '<?=base_url($controller."/save_common_settings") ?>',
			data: {
				"key_field":parameter,
				"key_value":parameter_value,
			},
			success: function(resultData) {
				alert("Save Complete");
				location.reload();
				}
			});
		});

	</script>
