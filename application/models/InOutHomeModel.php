<?php
class InOutHomeModel extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
	}
  public function menu() {
		return $this->db->get_where('menu',array());
	}
	//***********************common settings******************************** */
	/**
	 * select all data from  inout_settings
	 */
	public function select_key_data () {
		$data = $this->db->get_where('inout_settings',array())->result();
		if(count($data)>0) {
			return $data;
		}
	}

	/**
	 * save common settings value
	 * @param keyfield,keyvalue
	 */
	public function save_common_settings($post_data) {

		$data = [
			'key_value'=> $post_data['key_value']
		];
		$this->db->where('key_field',$post_data['key_field']);
		$this->db->update('inout_settings',$data);
	}
	//*************************************camera module********************************** */

	/**
	 * list for all inout_settings table fields
	 * table - inout_settings
	 * @param key_field
	 * @return key_value
	 */
	public function get_settings($field) {
		$data = $this->db->get_where('inout_settings',array())->result();
		if(count($data)>0) {
			foreach($data as $dt) {
				if($field==$dt->key_field) {
				return $dt->key_value;
				}
			}
		}
	}

	/**
	 * save camera
	 * table - inout_device
	 */
	public function save_camera($post_data) {
		if($post_data["id"]!=null) {
			$this->db->where('id',$post_data["id"]);
			$query = $this->db->update('inout_device', $post_data);
		}else {
			$query = $this->db->insert('inout_device', $post_data);
			// for update all other readings for current device
			$data['device_id'] = $post_data['name'];
			$query = $this->db->insert('people_count_lastreading', $data);
		}
    return $query ? true : false;
	}
	/**
	 * select camera_data
	 */
	public function select_camera($id=null) {
		if($id!=null) {
			$data = $this->db->get_where('inout_device',array('id'=>$id))->row();
		}else {
			$data = $this->db->get_where('inout_device',array())->result();
		}
		if($data!=null) {
				return $data;
		}
	}

	/**
	 * delete camera
	 */
	public function delete_camera($id) {

		$this->db->where('id',$id);
		$this->db->delete('inout_device');
	}
}
