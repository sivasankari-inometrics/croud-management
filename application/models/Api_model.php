<?php
class Api_model extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
	}
	public function people_count_settings($keyfield) {
		return $this->default_device = $this->db->get_where('people_count_settings',array('key_field'=>$keyfield))->row()->key_value;
	}
  public function get_device_data($data) {

		$today = date('Y-m-d');
		$device_id = $data['device_id'];
		$this->db->where('device_id',$device_id);
		$this->db->update('people_count_lastreading',$data);
		$this->db->insert('people_count_readings',$data);
	}

	public function set_device_data($data) {

		$datas['device_id'] = $data['device_id'];
		$flag = $data['flag'];
		if($flag=='1') {
			$this->db->where('key_field','default_device');
			$this->db->update('people_count_settings',$data['device_id']);
		}
		return $this->people_count_settings('default_interval');
	}
}
