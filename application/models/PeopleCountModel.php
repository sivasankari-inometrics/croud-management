<?php
class PeopleCountModel extends CI_Model{
  function __construct() {

		parent::__construct();
		$this->load->database();
		$this->device = $this->db->get_where('people_count_settings',array('key_field'=>'default_device'))->row()->key_value;
		$this->today = date('Y-m-d');
	}
  public function menu() {
		return $this->db->get_where('menu',array());
	}


	//********************************************** dashboard module****************************//
	public function get_default_device() {

		return $this->db->get_where('people_count_lastreading',array('device_id'=>$this->device))->row();

	}
	public function get_today_data() {

		$result = $this->db->query("SELECT count(*) as count,max(temperature) as max_temp FROM people_count_readings WHERE date(created_time)='$this->today'")->result();
		$data['capture_count'] = $result[0]->count;
		$data['max_temp'] = $result[0]->max_temp;
		return $data;
	}

	/**
	 * capture list
	 */
	public function capture_list() {
		//return $this->db->get_where('people_count_readings',array('device_id'=>$this->device))->result();
	}
	//********************************************** dashboard module****************************//

	//***********************common settings*************************************************//
	/**
	 * select all data from  people_count_settings
	 */
	public function select_key_data () {
		$data = $this->db->get_where('people_count_settings',array())->result();
		if(count($data)>0) {
			return $data;
		}
	}

	/**
	 * save common settings value
	 * @param keyfield,keyvalue
	 */
	public function save_common_settings($post_data) {

		$data = [
			'key_value'=> $post_data['key_value']
		];
		$this->db->where('key_field',$post_data['key_field']);
		$this->db->update('people_count_settings',$data);
	}
	//*************************************camera module********************************** */

	/**
	 * list for all people_count_settings table fields
	 * table - people_count_settings
	 * @param key_field
	 * @return key_value
	 */
	public function get_settings($field) {
		$data = $this->db->get_where('people_count_settings',array())->result();
		if(count($data)>0) {
			foreach($data as $dt) {
				if($field==$dt->key_field) {
					return $dt->key_value;
				}
			}
		}
	}

	/**
	 * save camera
	 * table - people_count_device
	 */
	public function save_camera($post_data) {
		if($post_data["id"]!=null) {
			$this->db->where('id',$post_data["id"]);
			$query = $this->db->update('people_count_device', $post_data);
		}else {
			$query = $this->db->insert('people_count_device', $post_data);
		}
    return $query ? true : false;
	}
	/**
	 * select camera_data
	 */
	public function select_camera($id=null) {
		if($id!=null) {
			$data = $this->db->get_where('people_count_device',array('id'=>$id))->row();
		}else {
			$data = $this->db->get_where('people_count_device',array())->result();
		}
		if($data!=null) {
				return $data;
		}
	}

	/**
	 * delete camera
	 */
	public function delete_camera($id) {

		$this->db->where('id',$id);
		$this->db->delete('people_count_device');
	}

}
